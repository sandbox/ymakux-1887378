<?php

/**
 * @file
 * Page callbacks for Commerce Webmoney.
 */

/**
 * Payment result callback.
 */
function commerce_webmoney_result_page_callback() {
  if (!empty($_POST)) {
    if (isset($_POST['LMI_PREREQUEST']) && $_POST['LMI_PREREQUEST'] == 1) {
      print 'YES';
    }

    $keys = array(
      'LMI_PAYEE_PURSE',
      'LMI_PAYMENT_AMOUNT',
      'LMI_PAYMENT_NO',
      'LMI_MODE',
      'LMI_SYS_INVS_NO',
      'LMI_SYS_TRANS_NO',
      'LMI_SYS_TRANS_DATE',
      'LMI_PAYER_PURSE',
      'LMI_PAYER_WM',
    );

    $has_error = FALSE;
    foreach ($keys as $key) {
      if ($key == 'LMI_MODE' && !isset($_POST[$key])) {
        $has_error = TRUE;
        break;
      }
      elseif ($key != 'LMI_MODE' && empty($_POST[$key])) {
        $has_error = TRUE;
        break;
      }
    }

    if (!$has_error) {
      if ($order = commerce_order_load($_POST['LMI_PAYMENT_NO'])) {
        $payment_method = commerce_payment_method_instance_load($order->data['payment_method']);
        $secret_key = $payment_method['settings']['secret_key'];

        $to_md5 = array(
          $_POST['LMI_PAYEE_PURSE'],
          $_POST['LMI_PAYMENT_AMOUNT'],
          $_POST['LMI_PAYMENT_NO'],
          $_POST['LMI_MODE'],
          $_POST['LMI_SYS_INVS_NO'],
          $_POST['LMI_SYS_TRANS_NO'],
          $_POST['LMI_SYS_TRANS_DATE'],
          $secret_key,
          $_POST['LMI_PAYER_PURSE'],
          $_POST['LMI_PAYER_WM'],
        );

        $to_md5 = implode('', $to_md5);
        $md5 = strtoupper(md5($to_md5));
        if ($md5 == $_POST['LMI_HASH']) {
          $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
          $currency_code = $order_wrapper->commerce_order_total->currency_code->value();
          $amount = $order_wrapper->commerce_order_total->amount->value();
          $amount = abs(commerce_currency_amount_to_decimal($amount, $currency_code));
          $purse = commerce_webmoney_purse($payment_method['settings']);

          $received_amount = abs($_POST['LMI_PAYMENT_AMOUNT']);
          $received_purse = $_POST['LMI_PAYEE_PURSE'];

          if ($amount == $received_amount && $purse == $received_purse) {
            commerce_webmoney_create_transaction($order, $order_wrapper);
          }
        }
      }
    }
  }
}

/**
 * Payment fail callback.
 */
function commerce_webmoney_fail_page_callback() {
  drupal_set_message(t('Payment unsuccessful!'), 'error');

  if (!empty($_POST['LMI_PAYMENT_NO'])) {
    if ($order = commerce_order_load($_POST['LMI_PAYMENT_NO'])) {
      commerce_payment_redirect_pane_previous_page($order);
      drupal_goto(commerce_checkout_order_uri($order));
    }
  }

  drupal_goto('<front>');
}

/**
 * Payment success callback.
 */
function commerce_webmoney_success_page_callback() {
  $keys = array(
    'LMI_PAYMENT_NO',
    'LMI_SYS_INVS_NO',
    'LMI_SYS_TRANS_NO',
    'LMI_SYS_TRANS_DATE',
  );

  $has_error = FALSE;
  foreach ($keys as $key) {
    if (empty($_POST[$key])) {
      $has_error = TRUE;
      break;
    }
  }

  if (!$has_error) {
    if ($order = commerce_order_load($_REQUEST['LMI_PAYMENT_NO'])) {
      drupal_set_message(t('Thank you, payment successful.'));
      commerce_payment_redirect_pane_next_page($order);
      drupal_goto(commerce_checkout_order_uri($order));
    }
  }

  drupal_set_message(t('Unknown error.'));
  drupal_goto('<front>');
}
