-- SUMMARY --

Provides a Webmoney payment method for Drupal Commerce.
After enabling the module, go to admin/commerce/config/payment-methods
and setting payment.

-- FEATURES --

Test account - for testing purposes only.
Live account - for processing real transactions.

-- REQUIREMENTS --

http://drupal.org/project/commerce
http://drupal.org/project/token
commerce_payment (in Drupal Commerce module)

-- INSTALLATION --

* Install as usual, see https://drupal.org/documentation/install/modules-themes/modules-7 for further information.

-- CONFIGURATION --

1) Open https://merchant.webmoney.ru/conf/purse.asp . Authorize. Select your site's purse and press "change".

2) Set settings for site's purse.
  2.1) Select Test/Work mode
  2.2) Set Trade Name.
  2.3) Result URL: http://YOUR_DOMAIN.COM/commerce/webmoney/result
  2.4.1) Success URL: http://YOUR_DOMAIN.COM/commerce/webmoney/success
  2.4.2) Select POST method if requesting Success URL
  2.5.1) Fail URL: http://YOUR_DOMAIN.COM/commerce/webmoney/fail
  2.5.2) Select POST method if requesting Fail URL
  2.6) Signature alfotirhm: MD5
  2.7) Press Save
  2.8) Copy Secret Key

3) Set settings on your website.
  3.1) Open http://YOUR_DOMAIN.COM/admin/commerce/config/payment-methods/manage/commerce_payment_commerce_webmoney
  3.2) Click on EDIT operation
  3.3) Select Payment mode (test/live)
  3.4) Submit your purse number
  3.5) Submit Secret Key from 2.8)

4) Save

-- CONTACT --

Current maintainer:
* ymakux https://drupal.org/user/2325368
* Harbuzau Yauheni - http://drupal.org/user/2123020
